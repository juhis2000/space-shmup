shader_type canvas_item;

uniform float alpha = 0;

void fragment() {
    vec4 texture_color = texture(TEXTURE, UV);
    COLOR = vec4(mix(texture_color.rgb, vec3(1,1,1), 0), alpha);
}