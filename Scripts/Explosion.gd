extends AnimatedSprite

func _ready():
	playing = true;
	get_node("/root/SoundManager").get_node("ExplosionSound").play(0);

func _on_Explosion_animation_finished():
	playing = false;
	queue_free();
