extends VisibilityNotifier2D

export var asteroids = false;
export var enemies = false;
export var boss = false;

func _ready():
	pass
	
func _process(delta):
	position.y += delta * 25;

func _on_VisibilityNotifier2D_screen_entered():
	var gui = get_tree().get_root().get_node("Main").get_node("GUI").get_node("Warnings");
	if(asteroids):
		gui.get_node("AsteroidsWarning").visible = true;
		gui.get_node("AsteroidsWarning").playing = true;
		queue_free();
	if(enemies):
		gui.get_node("EnemiesWarning").visible = true;
		gui.get_node("EnemiesWarning").playing = true;
		queue_free();
	if(boss):
		gui.get_node("BossWarning").visible = true;
		gui.get_node("BossWarning").playing = true;
		queue_free();
