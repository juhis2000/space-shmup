extends Sprite

var t = 0;

func _ready():
	pass

func _process(delta):
	if(material.get_shader_param("whitening") == 0.8):
		t += delta;
		if(t >= 0.1):
			t = 0;
			material.set_shader_param("whitening", 0);