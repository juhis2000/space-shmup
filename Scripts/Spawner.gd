extends Node

enum levels { idle, lvl1 };
var level;
var level_made = false;
onready var level_files = [preload("res://Levels/Level1.tscn")];

func _ready():
	level = levels.lvl1;
	
func _process(delta):
	match level:
		levels.lvl1:
			if(!level_made):
				level_made = true;
				var level = level_files[0].instance();
				get_parent().add_child(level);
				level = levels.idle; 
		levels.idle:
			pass


#extends Node
#
#var level;
#enum levels { lvl1 };
#
#onready var enemies = [preload("res://Scenes/Asteroid.tscn"), preload("res://Scenes/EnemyShip.tscn"),
#preload("res://Scenes/EnemyDrone.tscn")];
#var spawn_timer = 0;
#var spawn_speed = 3;
#
#func _ready():
#	level = levels.lvl1;
#	randomize();
#
#func _process(delta):	
#	match level:
#		levels.lvl1:
#			_spawn_timer(delta, spawn_speed);
#
#			if(spawn_timer == 0):
#				var numb = irand_range(0, enemies.size()-1);
#				var enemy = enemies[numb].instance();
#				_spawn(delta, enemy,
#				-get_viewport().get_visible_rect().size.x/2 + enemy.get_node("Sprite").texture.get_size().x,
#				get_viewport().get_visible_rect().size.x/2 - enemy.get_node("Sprite").texture.get_size().x,
#				-get_viewport().get_visible_rect().size.y/2 - enemy.get_node("Sprite").texture.get_size().y/4,
#				-get_viewport().get_visible_rect().size.y/2 - enemy.get_node("Sprite").texture.get_size().y/4,
#				irand_range(25, 100), irand_range(10, 15), 1);
#
#func _spawn_timer(delta, rate):
#	if(spawn_speed >= 1):
#		spawn_speed *= 0.9999;
#
#	spawn_timer += delta;
#	if(spawn_timer >= rate):
#		spawn_timer = 0;
#
#func irand_range(minVal, maxVal):
#	return (randi() % (maxVal - minVal + 1) + minVal);
#
#func _spawn(delta, node, minX, maxX, minY, maxY, speed, hp, dmg):
#		var enemy = node;
#		add_child(enemy);
#		enemy.position.x = rand_range(minX, maxX);
#		enemy.position.y = rand_range(minY, maxY);
#		enemy.spd = speed;
#		enemy.hp = hp;
#		enemy.dmg = dmg;