extends Line2D

var target;
var point;
#export(NodePath) var target_path;
export var trail_length = 0;
var original_trail_length;
export var fixed_pos = Vector2();

func _ready():
	original_trail_length = trail_length;
	#target = get_node(target_path);
	
func _process(delta):
	point = get_parent().get_parent().global_position + fixed_pos;
	add_point(point);
	while(get_point_count() > trail_length):
		remove_point(0);
	for i in get_point_count():
		if(get_point_position(i).y < get_parent().get_parent().global_position.y + fixed_pos.y):
			set_point_position(i, get_parent().get_parent().global_position + fixed_pos + Vector2(0, 5));