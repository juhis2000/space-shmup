extends Area2D

var hp = 1;
var max_hp = 0;
var spd = 1;
var norm_spd = 150;
var dmg = 1;
var original_pos;
var dir = 1;
var rng = 50;

var t = 0;

var shooting_delay = 0;
onready var bullet_node = preload("res://Scenes/EnemyBullet.tscn");

onready var tex1 = preload("res://Sprites/Enemies/Enemy2/enemy2.png");
onready var tex2 = preload("res://Sprites/Enemies/Enemy2/enemy3.png");

var sprites = [preload("res://Sprites/Enemies/Enemy2/enemy2.png")];
const explosion_scene = preload("res://Scenes/Explosion.tscn");

func _ready():
	max_hp = hp;
	original_pos = position.x
	add_to_group("Enemies");
	
	randomize();
	var nmb = irand_range(0, sprites.size()-1);
	$Sprite.texture = sprites[nmb];
	
func irand_range(minVal, maxVal):
	return (randi() % (maxVal - minVal + 1) + minVal);

func _process(delta):
	if(position.y < -192-16):
		spd = 25;
		position.y += spd * delta;
	else:
		spd = norm_spd;
		if(position.y > 300): queue_free();
		_whitening(delta);
		if(hp <= 0):
			_destroy();
		else:
			_movement(delta);

func _whitening(delta):
	t += delta;
	if(t >= 0.1):
		t = 0;
	if(t == 0):	
		if($Sprite.texture == tex1):
			$Sprite.texture = tex2;
			return;
		if($Sprite.texture == tex2):
			$Sprite.texture = tex1;
			return;
		
func _movement(delta):
	var direction = (get_tree().get_root().get_node("Main").get_node("PlayerShip").position - position).normalized();
	var motion = direction * spd * delta;
	position += motion

func _destroy():
	get_tree().get_root().get_node("Main").get_node("Camera2D")._shake();
	
	var explosion = explosion_scene.instance();
	explosion.position = position + Vector2(0, 20);
	get_tree().get_root().get_node("Main").add_child(explosion);
	
	get_tree().get_root().get_node("Main").get_node("PlayerShip").score += 1;
	get_tree().get_root().get_node("Main").get_node("GUI").get_node("ScoreText").text = "SCORE: " + String(get_tree().get_root().get_node("Main").get_node("PlayerShip").score);
	queue_free();