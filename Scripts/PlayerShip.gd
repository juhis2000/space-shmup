extends Node2D

var touch_one = {"down":false, "position":Vector2(0,0)}
var touch_two = {"down":false, "position":Vector2(0,0)}

const spd = 200;
var hp = 5;
var score = 0;
var motion = Vector2();

var burst_size = 5;
var burst_pos = [Vector2(-16,0), Vector2(-8,-3), Vector2(0,-5), Vector2(8,-3), Vector2(16,0)];
var burst_delay = 0;

var min_y;
var max_y;
var shooting_delay = 0;
var t = 0;

var dir = null;
var points = [];
onready var bullet_node = preload("res://Scenes/Bullet.tscn");
var ui;

var hp_bar; 

func _ready():
	hp_bar = get_parent().get_node("GUI").get_node("HealthBar").get_node("Bar");
	hp_bar.max_value = hp;
	hp_bar.value = hp;
		
	min_y = get_viewport().get_visible_rect().size.y/2-32;
	max_y = -get_viewport().get_visible_rect().size.y/2+32/2;
	
	ui = get_parent().get_node("GUI");

func _process(delta):
	$BulletPosRight.position = position + Vector2(8, -13);
	$BulletPosLeft.position = position + Vector2(-8, -13);
	_move(delta);
	_shoot(delta);
	_whitening(delta);
	_hp();
	
func _hp():
	if(hp <= 0):
		get_tree().reload_current_scene();

func _whitening(delta):
	if($Sprite.material.get_shader_param("whitening") == 0.8):
		t += delta;
		if(t >= 0.1):
			t = 0;
			$Sprite.material.set_shader_param("whitening", 0);

func _input(event):
	if(event is InputEventScreenTouch):
		if (event.index == 0):
			touch_one["down"] = event.is_pressed()
			touch_one["position"] = make_input_local(event).position;
		elif (event.index == 1):
			touch_two["down"] = event.is_pressed()
			touch_two["position"] = make_input_local(event).position;

func _move(delta):
	if (touch_one["down"] == true and touch_two["down"] == true):
		dir = "both";
		$Label.text = "Both";
	elif (touch_one["down"] == true or touch_two["down"] == true):
		var touch_position = null;
		
		if (touch_one["down"] == true):
			touch_position = touch_one["position"];
		elif (touch_two["down"] == true):
			touch_position = touch_two["position"];
		else:
			$Label.text = ("Error: Unknown touch!");
			return;
			
		if (touch_position.x < 0):
			dir = "left";
			$Label.text = "Left";
		else:
			dir = "right";
			$Label.text = "Right";
	else:
		dir = "none";
		$Label.text = "None";
	
	motion = Vector2();
	
	if((dir == "left" || Input.is_action_pressed("ui_left")) && position.x > -get_viewport().get_visible_rect().size.x/2+32/2):
		motion.x += -1;
		$Sprite.play("left");
	
	if((dir == "right" || Input.is_action_pressed("ui_right")) && position.x < get_viewport().get_visible_rect().size.x/2-32/2):
		motion.x += 1;
		$Sprite.play("right");
		
	if(position.y >= max_y && (dir == "both" || (Input.is_action_pressed("ui_right") && Input.is_action_pressed("ui_left")))):
		motion.x = 0;
		motion.y += -1.2;
		$Sprite.play("idle");
	elif(position.y <= min_y):
		motion.y += 0.5;
	if(motion.x == 0): $Sprite.play("idle");
	
	move_and_collide(motion * spd * delta);
	
func _shoot(delta):
	shooting_delay += delta;
	burst_delay += delta;
	if(shooting_delay >= 0.1):
		shooting_delay = 0;
	
	if(shooting_delay == 0 && (dir == "both" || (Input.is_action_pressed("ui_right") && Input.is_action_pressed("ui_left")))):
		if(burst_delay >= 0.5):
			burst_delay = 0;
		if(burst_delay == 0):
			for i in burst_size:
				get_node("/root/SoundManager").get_node("ShootingSound").play(0);
				var bullet = bullet_node.instance();
				bullet.get_node("Sprite").play("big");
				get_parent().add_child(bullet);
				bullet.position = position + burst_pos[i];
	else:
		if(shooting_delay == 0 && (dir == "right" || Input.is_action_pressed("ui_right"))):
			get_node("/root/SoundManager").get_node("ShootingSound").play(0);
			var bullet = bullet_node.instance();
			get_parent().add_child(bullet);
			bullet.position = $BulletPosRight.position;
		
		if(shooting_delay == 0 && (dir == "left" || Input.is_action_pressed("ui_left"))):
			get_node("/root/SoundManager").get_node("ShootingSound").play(0);
			var bullet = bullet_node.instance();
			get_parent().add_child(bullet);
			bullet.position = $BulletPosLeft.position;

func _on_Area2D_area_entered(area):
	if(area.is_in_group("Enemies")):
		get_tree().get_root().get_node("Main").get_node("GUI").get_node("EnemyHp").value = 0;
		area._destroy();
		area.get_node("Sprite").material.set_shader_param("whitening", 0.8);
		hp -= area.dmg;
		hp_bar.value = hp;
		area.queue_free();