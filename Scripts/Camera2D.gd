extends Camera2D

var shake = 0;
var magnitude = 10;

func _ready():
	pass

func _process(delta):
	offset = Vector2(rand_range(-shake, shake), rand_range(-shake, shake));
	shake *= 0.50;

func _shake():
	shake = magnitude;