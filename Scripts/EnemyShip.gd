extends Area2D

var hp = 5;
var max_hp = 0;
var spd = 1;
var norm_spd = 100;
var dmg = 1;
var original_pos;
var dir = 1;
var rng = 100;

var shooting_delay = 0;
onready var bullet_node = preload("res://Scenes/EnemyBullet.tscn");

var sprites = [preload("res://Sprites/Enemies/EnemyShip1/enemy_ship.png")];
onready var colliders = [$CollisionPolygon2D, $CollisionPolygon2D2];
const explosion_scene = preload("res://Scenes/Explosion.tscn");

func _ready():
	max_hp = hp;
	original_pos = position.x
	add_to_group("Enemies");
	
	randomize();
	var nmb = irand_range(0, sprites.size()-1);
	$Sprite.texture = sprites[nmb];
	colliders[nmb].disabled = false;
	
func irand_range(minVal, maxVal):
	return (randi() % (maxVal - minVal + 1) + minVal);

func _process(delta):
	if(position.y < -192-16):
		spd = 25;
		position.y += spd * delta;
	else:
		spd = norm_spd;
		if(position.y > 300): queue_free();
		_shooting(delta);
		if(hp <= 0):
			_destroy();
		else:
			_movement(delta);

func _movement(delta):
	if(position.y < -100):
		position.y += spd * delta;
	else:
		if(global_position.x >= rng): dir = -1;
		if(global_position.x <= -rng): dir = 1;
		position.x += dir * spd * delta;

func _shooting(delta):
	shooting_delay += delta;
	if(shooting_delay >= 1):
		shooting_delay = 0;
	
	if(shooting_delay == 0):
		get_node("/root/SoundManager").get_node("ShootingSound").play(0);
		var bullet = bullet_node.instance();
		get_parent().add_child(bullet);
		bullet.position = position;

func _destroy():
	get_tree().get_root().get_node("Main").get_node("Camera2D")._shake();
	
	var explosion = explosion_scene.instance();
	explosion.position = position + Vector2(0, 20);
	get_tree().get_root().get_node("Main").add_child(explosion);
	
	get_tree().get_root().get_node("Main").get_node("PlayerShip").score += 1;
	get_tree().get_root().get_node("Main").get_node("GUI").get_node("ScoreText").text = "SCORE: " + String(get_tree().get_root().get_node("Main").get_node("PlayerShip").score);
	queue_free();