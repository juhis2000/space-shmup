extends Area2D

var spd = 300;
var dmg = 1;
var anim_finished = false;
var d;
var t = 0;
var hasHit = false;

func _ready():
	$Sprite.playing = true;

func _process(delta):
	d = delta;
	if(anim_finished):
		queue_free();
	position.y += spd * delta;

func _on_VisibilityNotifier2D_screen_exited():
	queue_free();

func _on_Bullet_area_entered(area):
	if(area.get_parent().name == "PlayerShip" && !hasHit):
		hasHit = true;
		area.get_parent().hp -= dmg;
		area.get_parent().hp_bar.value = area.get_parent().hp;
		
		area.get_parent().get_node("Sprite").material.set_shader_param("whitening", 0.8);
		$Sprite.play("hit");
		get_node("/root/SoundManager").get_node("HitSound").play(0);
		spd = 0;

func _on_Sprite_animation_finished():
	if($Sprite.animation == "hit"):
		anim_finished = true;
