extends Area2D

var hp = 10;
var max_hp = 0;
var spd = 1;
var norm_spd = 75;
var dmg = 1;
var rot_dir = 0;
var rot_speed = 0;

var sprites = [preload("res://Sprites/Asteroids/large_asteroid.png"),
preload("res://Sprites/Asteroids/medium_asteroid.png")];
onready var colliders = [$CollisionPolygon2D, $CollisionPolygon2D2];
const explosion_scene = preload("res://Scenes/Explosion.tscn");

func _ready():
	add_to_group("Enemies");
	
	randomize();
	var nmb = irand_range(0, sprites.size()-1);
	$Sprite.texture = sprites[nmb];
	colliders[nmb].disabled = false;
	rotation_degrees = rand_range(0, 359);
	rot_dir = irand_range(-1, 1);
	rot_speed = rand_range(0, 1);
	if(nmb == 1): hp = 5;
	max_hp = hp;
	
func irand_range(minVal, maxVal):
	return (randi() % (maxVal - minVal + 1) + minVal);

func _process(delta):
	if(position.y < -192-32):
		spd = 25;
		position.y += spd * delta;
	else:
		spd = norm_spd;
		rotation_degrees += rot_dir * rot_speed;
		if(position.y > 300): queue_free();
		if($Sprite.texture == sprites[1] && hp > 5): hp = 5;
		if(hp <= 0):
			_destroy();
		else:
			position.y += spd * delta;
		
func _destroy():
	get_tree().get_root().get_node("Main").get_node("Camera2D")._shake();
	
	var explosion = explosion_scene.instance();
	explosion.position = position + Vector2(0, 20);
	get_tree().get_root().get_node("Main").add_child(explosion);
	
	get_tree().get_root().get_node("Main").get_node("PlayerShip").score += 1;
	get_tree().get_root().get_node("Main").get_node("GUI").get_node("ScoreText").text = "SCORE: " + String(get_tree().get_root().get_node("Main").get_node("PlayerShip").score);
	queue_free();