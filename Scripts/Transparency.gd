extends AnimatedSprite

var t = 0;
var dir = 1;
var a = 1;
var mySin = 1;

func _process(delta):
	print(sin(delta))
	mySin = sin(delta * mySin + 1) / 2;
	#print(mySin)
	
	if(visible):
		
		t += delta;
		if(t <= 5):
			a += 0.02 * dir;
			if(a >= 1):
				dir = -1
			if(a <= 0.1):
				dir = 1;
			modulate.a = a;
		else:
			a -= 0.03;
			modulate.a = a;
			if(a <= 0): queue_free();