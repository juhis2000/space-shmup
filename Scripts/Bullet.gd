extends Area2D

var spd = 500;
var dmg = 1;
var anim_finished = false;
var d;
var t = 0;

func _ready():
	pass

func _process(delta):
	d = delta;
	if(anim_finished):
		queue_free();
	position.y -= spd * delta;

func _on_VisibilityNotifier2D_screen_exited():
	queue_free();

func _on_Bullet_area_entered(area):
	if(area.is_in_group("Enemies") && get_parent().name != "Spawner"):
		var enemy_hp = get_tree().get_root().get_node("Main").get_node("GUI").get_node("EnemyHp");
		enemy_hp.max_value = area.max_hp;
		enemy_hp.value = area.hp;
		enemy_hp.value -= dmg;
		
		area.get_node("Sprite").material.set_shader_param("whitening", 0.8);
		area.hp -= dmg;
		$Sprite.play("hit");
		get_node("/root/SoundManager").get_node("HitSound").play(0);
		spd = 0;

func _on_Sprite_animation_finished():
	anim_finished = true;
