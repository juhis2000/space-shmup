Space Shoot 'Em Up is a game created in Godot 3 using GDScript for mobile
devices. All the sprites and code are made by me. Project's goal was to
practice mobile development in Godot, especially touch screen controlling.

Gameplay  
Avoid the enemy space ships and asteroids that fly vertically towards you and
score points by destroying them.

Controls  
Left/Right side of the screen: move left/right  
Both sides pressed: shoot a group of 5 bullets

To play the game download the repository and open the project file in Godot 3.  
Press f5 and you should be good to go. Game builds not available, tested in
Android 6+ and Windows 10.

Video from some part of the project:
![](video.mp4)

See also https://godotengine.org/license